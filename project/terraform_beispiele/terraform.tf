terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
    google = {
      source = "hashicorp/google"
    }
  }
  required_version = ">= 0.13"
//  backend "s3" {
//    bucket = "mybucket"
//    key    = "path/to/my/key"
//    region = "eu-central-1"
//  }
}

provider "aws" {
  region = "eu-central-1"
}
provider "aws" {
  alias = "west"
  region = "eu-west-1"
}

resource "aws_instance" "my_instance" {
  ami           = "your_amazon_machine_image"
  instance_type = "m5a.large"
}

data "aws_ami" "amazon-linux-2" {
  most_recent = true
  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
  owners = ["amazon"]
}

variable "my_variable" {
  type = string
  default = "a_default_value"
  description = "dummy variable"
  validation {
    condition = var.my_variable != ""
    error_message = "The Variable must not be empty."
  }
  sensitive = true

}

output "ec2_instance" {
  value = aws_instance.my_instance
  description = "My new EC2 instance"
}

module "my_ec2_module" {
  source = "ec2"

  my_variable = data.aws_ami.amazon-linux-2.id
}

