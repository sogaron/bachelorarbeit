
//locals {
//  settings = yamldecode(file("../settings.yml"))
//}

module "ecs-service" {
  source = "../ecs-service"
  stage = "test"
  app_name = local.settings.app_name
  image_registry_secret_name = local.settings.secret_name
  secrets = var.secrets
  cluster_id = data.aws_ecs_cluster.ecs_cluster.id
  cluster_name = local.settings.ecs_name
  image_url = local.settings.image_url
}

data "aws_ecs_cluster" "ecs_cluster" {
        cluster_name = local.settings.ecs_name
}

variable "secrets" {
  type = list(object({
    name = string,
    value = string
  }))
  sensitive = true
}

locals {
  settings = {
    app_name: local.settings.app_name
    secret_name: local.settings.secret_name
    ecs_name: local.settings.ecs_name
    image_url: local.settings.image_url
  }
}