
resource "aws_ecs_service" "ecs_service" {
  name = local.service_name
  cluster = var.cluster_id
  task_definition = aws_ecs_task_definition.ecs_task.arn
  desired_count = 1
  launch_type = var.launch_type

  dynamic "network_configuration" {
    for_each = var.launch_type == "FARGATE" ? [""] : []
    content {
      assign_public_ip = false
      # out of scope
      subnets = data.aws_subnet_ids.private.ids
      # out of scope
      security_groups = [data.aws_security_group.default.id]
    }
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.application_target_group.arn
    container_name = local.service_name
    container_port = 8080
  }
}

data "aws_subnet_ids" "private" {
  vpc_id = data.aws_security_group.default.vpc_id
}

data "aws_security_group" "default" {
  name = "my_group"
}

