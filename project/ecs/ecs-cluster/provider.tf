
provider "aws" {
  region = "eu-central-1"
  assume_role {
    role_arn = "arn:aws:iam::012345678910:role/Maintainer"
  }
}

terraform {
  backend "s3" {
    region = "eu-central-1"
    bucket = "generali-m8sand-dev-eu-ecentral-1-default"
    key = "terraform/ecs-cluster.tfstate"
    role_arn = "arn:aws:iam::012345678910:role/Maintainer"
  }
}

