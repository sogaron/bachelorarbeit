
resource "aws_lb" "alb_intern" {
  name = "${var.name}-intern"
  internal = true
  load_balancer_type = "application"
  tags = {"generali:dns" : lower("${var.name}-private.ecs")}
  access_logs {
    bucket = data.aws_s3_bucket.log.bucket
    prefix = "alb"
    enabled = true
  }
}

resource "aws_lb_listener" "listener_443" {
  load_balancer_arn = aws_lb.alb_intern.arn
  port = 443
  protocol = "HTTPS"
  default_action {
    type = "fixed_response"
    fixed_response {
      content_type = "text/plain"
      message_body = "This Path does not lead to any application."
      status_code = "200"
    }
  }
}

resource "aws_lb_listener" "listener_80" {
  load_balancer_arn = aws_lb.alb_intern.arn
  port = 80
  protocol = "HTTP"
  default_action {
    type = "redirect"
    redirect {
      status_code = "HTTP_301"
      port = "443"
      protocol = "HTTPS"
    }
  }
}

data "aws_s3_bucket" "log" {
  bucket = ""
}
