
resource "aws_ecs_cluster" "cluster" {
  name = var.name
  # out of scope
  capacity_providers = [aws_ecs_capacity_provider.autoscaling_provider.name]
  #out of scope
  default_capacity_provider_strategy {
    capacity_provider = aws_ecs_capacity_provider.autoscaling_provider.name
  }
}

resource "aws_ecs_capacity_provider" "autoscaling_provider" {
  name = "${var.name}-capacity-provider"
  auto_scaling_group_provider {
    auto_scaling_group_arn = aws_autoscaling_group.autoscaling_group.arn
    managed_scaling {
      target_capacity = 80
    }
  }
}
resource "aws_autoscaling_group" "autoscaling_group" {
  name = "ECS-${var.name}"
  max_size = 2
  min_size = 0
  launch_template {
    id = aws_launch_template.ecs_template.id
  }
}
resource "aws_launch_template" "ecs_template" {
  name = "ECS-${var.name}"
  # id of the ami you want to use
  image_id = data.aws_ami.ecs_image.image_id
  # the
  instance_type = "a instance"
}

data "aws_ami" "ecs_image" {
  filter {
    name = "name"
    values = ["linux_image_aue"]
  }
  owners = []
}
