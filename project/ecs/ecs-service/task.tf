
locals {
  service_name = "${var.app_name}-${var.stage}"
  spring_profiles = [{name = "spring_profiles_active", value = var.stage}]
  servlet_context_path = [{name = "server_servlet_context_path", value = "/${local.service_name}"}]
  environments = jsonencode(concat(var.secrets, local.spring_profiles,local.servlet_context_path))
}

resource "aws_ecs_task_definition" "ecs_task" {
  family = "${local.service_name}"
  network_mode = "bridge"
  cpu = "512"
  memory = "1024"
  
  container_definitions = <<EOF
[
  {
    "name": "${local.service_name}"
    "image": "${var.image_url}",
    "repositoryCredentials": {
      "credentialsParameter": "${data.aws_secretsmanager_secret.registry_credentials.arn}"
    },
    "environment": ${local.environments},
    "portMappings": [
      {
        "containerPort": 8080,
        "hostPort": 0,
        "protocol": "tcp"
      }
    ]
  }
]
EOF
}

data "aws_secretsmanager_secret" "registry_credentials" {
  name = var.image_registry_secret_name
}
