
resource "aws_lb_target_group" "ec2" {
  name = local.service_name
  target_type = "instance"
  health_check {
    enabled = true
    path = "/${local.service_name}/actuator/health"
    protocol = "HTTP"
  }
}

resource "aws_lb_listener_rule" "ec2" {
  listener_arn = data.aws_alb_listener.listener_443.arn
  action {
    type = "forward"
    target_group_arn = aws_lb_target_group.ec2.arn
  }
  condition {
    path_pattern {
      values = ["/${local.service_name}/*"]
    }
  }
}

data "aws_alb_listener" "listener_443" {
  load_balancer_arn = data.aws_alb.alb.arn
  port = 443
}

data "aws_alb" "alb" {
  name = "${var.cluster_name}-private"
}