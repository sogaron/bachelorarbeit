variable "image_registry_secret_name" {
  type = string
}
variable "app_name" {
  type = string
}
variable "stage" {
  type = string
}
variable "image_url" {
  type = string
}
variable "secrets" {
  type = list(object({
    name = string,
    value = string
  }))
  sensitive = true
}
variable "cluster_id" {
  type = string
}
variable "cluster_name" {
  type = string
}