
resource "aws_ecs_service" "ecs_service" {
  name = local.service_name
  cluster = var.cluster_id
  task_definition = aws_ecs_task_definition.ecs_task.arn
  desired_count = 1
  launch_type = "EC2"
  load_balancer {
    target_group_arn = aws_lb_target_group.ec2.arn
    container_name = local.service_name
    container_port = 8080
  }
}

