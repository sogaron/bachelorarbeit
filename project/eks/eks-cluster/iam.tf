resource "aws_iam_role" "eks_cluster_admin" {
  name = "EKS-Cluster-Admin"
  assume_role_policy = data.aws_iam_policy_document.admin.json
}

resource "aws_iam_role" "eks_cluster_nodes" {
  name = "EKS-Cluster-Nodes"
  assume_role_policy = data.aws_iam_policy_document.nodes.json
}

data "aws_iam_policy_document" "admin" {
  statement {
    effect = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type = "AWS"
      identifiers = ["arn:aws:iam::012345678910:role/Maintainer"]
    }
  }
}

data "aws_iam_policy_document" "nodes" {
  statement {
    effect = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}