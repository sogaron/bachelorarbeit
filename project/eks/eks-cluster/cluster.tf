
resource "aws_eks_cluster" "cluster" {
  name = var.name
  version = "1.21"
  # out of scope
  role_arn = aws_iam_role.eks_cluster_admin.arn
  vpc_config {
    # out of scope
    subnet_ids = data.aws_subnet_ids.private.ids
  }
}

data "aws_subnet_ids" "private" {
  vpc_id = data.aws_security_group.default.vpc_id
}

data "aws_security_group" "default" {
  name = "my_group"
}
