
resource "aws_route53_record" "default" {
  name = "*.eks.my-scope.dev.eu-central-1.aws.generali-cloud.net"
  type = "A"
  zone_id = aws_lb.alb_intern.zone_id
  alias {
    name = aws_lb.alb_intern.dns_name
    zone_id = aws_lb.alb_intern.zone_id
    evaluate_target_health = true
  }
}


