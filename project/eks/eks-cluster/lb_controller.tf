
provider "kubernetes" {
  host = aws_eks_cluster.cluster.endpoint
}

resource "kubernetes_deployment" "ingress_controller" {
  metadata {
    name = "ingress-controller"
    # out of scope
    namespace = kubernetes_namespace.ingress_controller.metadata[0].name
  }
  spec {
    replicas = 1
    template {
      spec {
        container {
          name = "ingress-controller"
          image = "asdasdkljhdfdgsdfgfsdgsdfgdsfgsdfgsdfgdfgsd:latest"
        }
      }
      metadata {}
    }
  }
}

resource "kubernetes_namespace" "ingress_controller" {
  metadata {
    name = "ingress-controller"
  }
}
