resource "aws_eks_node_group" "default" {
  cluster_name = aws_eks_cluster.cluster.name
  node_group_name = "default"
  # out of scope
  node_role_arn = aws_iam_role.eks_cluster_nodes.arn
  # out of scope
  subnet_ids = data.aws_subnet_ids.private.ids
  scaling_config {
    desired_size = 1
    max_size = 2
    min_size = 1
  }am
  launch_template {
    # out of scope
    id = aws_launch_template.eks_template.id
    # out of scope
    version = aws_launch_template.eks_template.latest_version
  }
}

resource "aws_launch_template" "eks_template" {
  name = "EKS-${aws_eks_cluster.cluster.name}"
  # id of the ami you want to use
  image_id = data.aws_ami.ecs_image.image_id
  # the
  instance_type = "a instance"
}

data "aws_ami" "ecs_image" {
  filter {
    name = "name"
    values = ["linux_image_aue"]
  }
  owners = []
}