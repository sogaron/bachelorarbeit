## Two-sided layout for reports or thesis at the FH Aachen
This is a two-sided layout for reports or final thesis at the FH Aachen. It was used for a thesis in the faculty of Medical Engineering and Applied Mathematics in 2019. The structure and requirements should always be checked with the professor and the examination office. 
The structures was adapted from Sven Hinz (https://github.com/SvenHinz). 

Comments in the document describe the functions of the code. 
The code was developed in Overleaf (\url{https://www.overleaf.com)}. Overleaf is an online tool to write and compile your LaTeX document. An account, but no installation is needed. 

It is also possible to install LaTeX on your desktop:

- [Mac](https://tug.org/mactex/ "MacTeX")
- [Windows](http://miktex.org/ "MiKTeX")
- [Ubuntu](https://wiki.ubuntuusers.de/LaTeX/ "Anleitung für Ubuntu")

Additional a LaTeX-Editor is useful:

- [Texmaker](http://www.xm1math.net/texmaker/ "Texmaker") (cross-platform)
- [TeXstudio](http://www.texstudio.org/ "TeXstudio") (cross-platform)
- [TeXnicCenter](http://www.texniccenter.org/ "TeXnicCenter") (Windows only)
